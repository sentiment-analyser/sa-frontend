### Проект для хранения front кода sentiment-analyser

#### В данном проекте реализовано:
- Хранение кода фронт приложения
- Сборка образа docker
- Пуш образа в docker hub

#### Запуск проекта локально 
` $ yarn start `

#### Сборка приложения
` $ yarn build `

#### Сборка контейнера
` $ docker build -f Dockerfile -t $DOCKER_USER_ID/sentiment-analysis-frontend . `

#### Запуск контейнера
` $ docker run -d -p 80:80 $DOCKER_USER_ID/sentiment-analysis-frontend `

#### Публиация контейнера
` $ docker push $DOCKER_USER_ID/sentiment-analysis-frontend `